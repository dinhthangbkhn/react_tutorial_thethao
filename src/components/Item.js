import React,{Component} from 'react';
import '../css/Item.css';
import '../library/bootstrap/css/bootstrap.min.css';
class Item extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="item row">
                <hr/>
                <h4 className="title">{this.props.title}</h4>
                <img className="col-md-4"src={this.props.image}/>
                <div className="text col-md-6">
                    <div className="content">
                        <p>
                            {this.props.content}
                        </p>
                    </div>
                </div>
                <div className="col-md-2"></div>
            </div>
        )
    }
}

export default Item;
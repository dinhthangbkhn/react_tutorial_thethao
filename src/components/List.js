import React,{Component} from 'react';
import Item from './Item.js';
import '../css/List.css';
import '../library/bootstrap/css/bootstrap.min.css';
import data from '../data/data.js';
import ReactPaginate from 'react-paginate';

class List extends Component {
    constructor(props) {
        super(props);
        this.state = {
            perPage: 3,
            pageCount: 3,
            list:data.slice(0,3)
        };
        this.handlePageClick = this.handlePageClick.bind(this);
    }
    handlePageClick(click) {
        let begin = this.state.perPage*click.selected;
        let end = begin+3;
        let newdata = data.slice(begin,end);
        this.setState({list: newdata});
    }
    render() {
        // let list =[];
        let list = this.state.list.map((item)=>
            <Item key={item.title} title={item.title} content={item.content} image={item.image}/>
        );
        return (
            <div>
            <div className="list">
                {list}
            </div>  
            <ReactPaginate previousLabel={"previous"}
                       nextLabel={"next"}
                       breakLabel={<a href="">...</a>}
                       breakClassName={"break-me"}
                       pageCount={this.state.pageCount}
                       marginPagesDisplayed={1}
                       pageRangeDisplayed={3}
                       onPageChange={this.handlePageClick}
                       containerClassName={"pagination"}
                       subContainerClassName={"pages pagination"}
                       activeClassName={"active"} />
            </div>
             
        )
    }
}

export default List;
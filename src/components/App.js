import React, { Component } from 'react';
import logo from '../logo.svg';
import '../css/App.css';
import List from './List.js';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to The Thao</h2>
        </div>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <div className="row">
          <div className="col-md-1"></div>
          <div className="col-md-5">
            <List />
          </div>
          <div className="col-md-5"></div>
        </div>

        
      </div>
    );
  }
}

export default App;

let data = [
    {
        title: "Arbeloa: 'Ronaldo không thể kiếm đội nào hay hơn Real'",
        image: "http://img.f2.thethao.vnecdn.net/2017/06/30/rona-6293-1498804546_180x108.jpg",
        content: "Cựu cầu thủ của Real Madrid lên tiếng khuyên đồng đội cũ nên ở lại sân Bernabeu."
    },
    {
        title: "Xavi, Eto’o đi chuyên cơ tới dự đám cưới Messi",
        image: "http://img.f1.thethao.vnecdn.net/2017/06/30/2-1498805733_180x108.jpg",
        content: "Hai đồng đội cũ ở Barca cùng nhiều ngôi sao khác đi chuyên cơ tới Argentina dự đám cưới của Lionel Messi và bạn gái Roccuzzo."
    },
    {
        title: "Ronaldo khoe ảnh hai con mới",
        image: "http://img.f1.thethao.vnecdn.net/2017/06/30/ronaldo-khoe-anh-hai-con-moi-1498789136_180x108.jpg",
        content: "CR7 đăng ảnh bế hai con sinh đôi sau khi đến thăm lần đầu tiên."
    },
    {
        title: "Siêu sao bóng rổ được đặc cách tham dự giải golf chuyên nghiệp",
        image: "http://img.f4.thethao.vnecdn.net/2017/06/30/Untitled-2-8167-1498810882_180x108.jpg",
        content: "Tranh cãi đã nổ ra khi Stephen Curry nhận được vé đặc cách tham dự Ellie Mae Classic, một sự kiện thuộc Web.com Tour."
    },
    {
        title: "Ronaldinho trở lại phòng thay đồ Barca",
        image: "http://img.f1.thethao.vnecdn.net/2017/06/30/1-1498809369_180x108.jpg",
        content: "Cựu danh thủ Brazil cùng nhiều ngôi sao cũ của Barca tái xuất trong phòng thay đồ để chuẩn bị chơi trận giao hữu với các huyền thoại Man Utd."
    },
    {
        title: "Venus Williams liên quan đến tai nạn chết người",
        image: "http://img.f3.thethao.vnecdn.net/2017/06/30/Untitled-1-3861-1498798392_180x108.jpg",
        content: "Tay vợt kỳ cựu người Mỹ bị cảnh sát cho là nguyên nhân gây ra cái chết của một cụ ông 78 tuổi vài ngày trước Wimbledon."
    },
    {
        title: "Chile và những màn đá luân lưu gieo sầu cho Messi - Ronaldo",
        image: "http://img.f2.thethao.vnecdn.net/2017/06/29/5-1498712864_180x108.jpg",
        content: "Trong ba năm liên tiếp, tuyển Chile giành ba chiến thắng trước các đối thủ lớn, và đoạt hai danh hiệu bằng thi đá 11m."
    },
    {
        title: "Con trai Zidane chia tay Real, gia nhập đội ở La Liga ",
        image: "http://img.f3.thethao.vnecdn.net/2017/06/30/enzo-zidane-1480542872-800-4754-1498779727_180x108.jpg",
        content: "Tiền vệ Enzo Zidane Fernandez đầu quân cho Alaves theo hợp đồng có thời hạn ba năm."
    },
    {
        title: "CĐV Real ủng hộ phương án bán Bale, thay bằng Mbappe",
        image: "http://img.f2.thethao.vnecdn.net/2017/06/30/mbappe-9851-1498776357_180x108.jpg",
        content: "Đa số CĐV Real được hỏi cho rằng, tiền đạo 18 tuổi Kylian Mbappe Lottin là thay thế xứng đáng nếu bán được Bale."
    }
];
export default data;